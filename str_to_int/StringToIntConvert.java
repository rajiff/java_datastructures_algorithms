
// Questions
// only signed integers  2147483646 ?
// can expect null, empty ?
// limit to max of integer value ?

class StringToIntConvert {
	
	static int convertStringToInt(String str) {
		int intVal = -1;
		// will put null check before because check is from left to right
		if(str == null || str.equals(""))
			return intVal;
		
		// Start from zero
		intVal = 0;
		
		for (int i= str.length() -1, decimal = 0; i >= 0 ; i--, decimal++) {
			int currentDigit = str.charAt(i) - '0';
			// System.out.println("Current digit: " + currentDigit);
			
			int currentDecimalAdjustedVal = currentDigit * ((int)Math.pow(10, decimal));
			// System.out.println("After decimal adjust: " + currentDecimalAdjustedVal);
				
			intVal += currentDecimalAdjustedVal;
			// System.out.println("Integer value: " + intVal);
		}

		return intVal;
	}
	
	public static void main(String[] args) {
		System.out.println("Integer converted value for 123 " + convertStringToInt("123")); // Expecting the integer equivalent of 123
		System.out.println("Integer converted value for empty string " + convertStringToInt("")); // Expecting -1
		System.out.println("Integer converted value for null " + convertStringToInt(null)); // Expecting -1
		System.out.println("Integer converted value for 987654321 " + convertStringToInt("987654321")); // Expecting the integer equivalent of 123
		System.out.println("Integer converted value for 2147483646 " + convertStringToInt("2147483646")); // Expecting the integer equivalent of 123
		
		// overflowing integer range
		System.out.println("Integer converted value for 987654321987654321 " + convertStringToInt("987654321987654321")); // Expecting the integer equivalent of 123
		
	}
}