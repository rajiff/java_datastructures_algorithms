import java.util.Scanner;

class ReadInputFromConsole {
	public static void main(String[] input) {
		// Read from STDIN
		// PARSE it into a variable
		// Consider each line is a separate input
		
		// initialize scanner
		Scanner scanner = new Scanner(System.in);
		
		// Assuming to read the entire line as String
		String strLine1 = scanner.nextLine();
		String strLine2 = scanner.nextLine();
		
		// Next will give back input as soon as a string is ended, which is either by space or new line
		// String strLine1 = scanner.next();
		// String strLine2 = scanner.next();
		
		// PS scanner treats comma as a input separator
		// When done reading expected number of inputs, lets close the scanner listening from input
		scanner.close();
		
		System.out.println("Line 1 " + strLine1);
		System.out.println("Line 2 " + strLine2);
	}
}