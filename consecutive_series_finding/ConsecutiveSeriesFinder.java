import java.util.Arrays;

public class ConsecutiveSeriesFinder {

	static int[] commaDelimStrToIntArray(String commaDelimStr) {
		return Arrays.stream(commaDelimStr.split(",")).mapToInt(Integer::parseInt​).toArray();
	}

	static String checkForConsecutive(String series) {
		// convert the comma separated string to array of numbers
		// sort the array
		// loop through the array to find the numerical difference between two consecutive array elements
		// if the difference between two consecutive array element is more than -1 or 1, declare it is not a consecutive number 

		String result = "are non consecutive numbers";
		// Handle scenarios like empty string, null string, 
		if(series == null || series.equals(""))
			return result;
		else {
			// Assume that result is consecutive unless it is set explicitly to non consecutive while looping. 
			result = "are consecutive numbers";

			int[] numArray = commaDelimStrToIntArray(series);
			Arrays.sort(numArray);
		
			// Loop from 0th element to n-1th element
			for(int i=0; i < numArray.length - 1; i++) {
				if( Math.abs(numArray[i] - numArray[i+1]) != 1) {
					result = "are non consecutive numbers";
					break;
				} 
			}
		}

		return result;
	}
	
	public static void main(String[] args) {
		System.out.println("Series 98,96,95,94,93 " + checkForConsecutive("98,96,95,94,93"));
		System.out.println("Series 54,53,52,51,50,49,48 " + checkForConsecutive("54,53,52,51,50,49,48"));
		System.out.println("Series 1,2,3,4,5,6,6 " + checkForConsecutive("1,2,3,4,5,6,6"));
	}
}
