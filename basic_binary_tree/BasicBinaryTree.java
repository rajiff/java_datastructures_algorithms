
class Node {
	int data;
	Node left;
	Node right;
	
	Node(int data) {
		this.data = data;
		
		// child nodes will be initially null
		this.left = null;
		this.right = null;
	}
}

class BasicTree {
	Node root; 
	
	public Node insertNewNode(int[] dataValues, Node root, int level) {
		if (level < dataValues.length) {
			root = new Node(dataValues[level]);
			
			root.left = insertNewNode(dataValues, root.left, (2 * level + 1));
			root.right = insertNewNode(dataValues, root.right, (2 * level + 2));	
		}
		return root; 
	}
	
	int height(Node root) {
		if(root == null)
			return 0;
		else
			return Math.max(height(root.left), height(root.right)) + 1;
	}
	
	// In Order "Left-Subtree => Root => Right-SubTree"
	public void traverseInOrder(Node root) {
		if (root != null) {
			System.out.print("[");
			traverseInOrder(root.left);
			System.out.print(" " + root.data + " ");
			traverseInOrder(root.right);
			System.out.print("]");
		}
	}
	
	// Pre Order "Root => Left-Subtree => Right-SubTree"
	public void traversePreOrder(Node root) {
		if (root != null) {
			System.out.print("[");
			System.out.print(" " + root.data + " ");
			traversePreOrder(root.left);
			traversePreOrder(root.right);
			System.out.print("]");
		}
	}
	
	// Post Order "Left-Subtree => Right-SubTree => Root"
	public void traversePostOrder(Node root) {
		if (root != null) {
			System.out.print("[");
			traversePostOrder(root.left);
			traversePostOrder(root.right);
			System.out.print(" " + root.data + " ");
			System.out.println("]");
		}
	}
}

// Reference 
// - https://www.geeksforgeeks.org/construct-complete-binary-tree-given-array/ 
// - https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
// - https://www.geeksforgeeks.org/binary-search-tree-set-1-search-and-insertion/

public class BasicBinaryTree {
	public static void main(String[] args) {
		// Program to construct the binary tree and do all 3 types of traversal
		BasicTree newTree = new BasicTree();
		// Taking a sorted array		
		int[] data	= new int[]{1, 2, 3, 4, 5, 6};
		newTree.root = newTree.insertNewNode(data, newTree.root, 0); 
		
		// int[] data	= new int[]{1, 2, 3, 4, 5, 6, 7, 8};
		//newTree.root = newTree.insertNewNode(data, newTree.root, 0); 
		
		// int [] preOrderData = new int[]{1, 2, 4, 8, 5, 3, 6, 7};
		// newTree.root = newTree.insertNewNode(preOrderData, newTree.root, 0); 
		
		System.out.println("The height of the tree is " + newTree.height(newTree.root));
		
		System.out.println("\n===========================\nIn Order\n===========================\n");
		newTree.traverseInOrder(newTree.root);
		
		System.out.println("\n===========================\nPre Order\n===========================\n");
		newTree.traversePreOrder(newTree.root);
		
		System.out.println("\n===========================\nPost Order\n===========================\n");
		newTree.traversePostOrder(newTree.root);
	}
}