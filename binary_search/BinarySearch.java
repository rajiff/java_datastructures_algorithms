import java.util.Arrays;

class BinarySearch {
	// Return index of the data found in the array
	static int binarySearch(int[] numArray, int numToFind) {
		// This to work, the given array must be sorted in ascending order
		int numAtIndex = -1;
		
		if(numArray.length > 0) {
			int left = 0;
			int right = numArray.length - 1;
			
			while(right >= left) {
				int mid = (left + right)/2;
				
				// System.out.println("Finding number from " + left + " to " + right + " with mid as " + mid);
				
				if(numArray[mid] == numToFind) {
					numAtIndex = mid; 
					break;
				}
				
				if(numArray[mid] < numToFind)
					left = mid + 1;
				else
					right = mid - 1;
			}
		}
		
		return numAtIndex;
	}
	
	public static void main(String[] args) {
		System.out.println("Binary Search ...!");
		System.out.println("In Array {1, 2, 3, 4, 5, 6, 9, 10} Data 5 found at " + binarySearch(new int[]{1, 2, 3, 4, 5, 5, 6, 9, 10}, 5));
		System.out.println("In Array {1, 2, 3, 4, 5, 5, 6, 9, 10, 11, 14, 15, 17, 19, 21, 25, 27} Data 17 found at " + binarySearch(new int[]{1, 2, 3, 4, 5, 5, 6, 9, 10, 11, 14, 15, 17, 19, 21, 25, 27}, 17));
		System.out.println("In Array {} Data 33 found at " + binarySearch(new int[]{}, 33));
		System.out.println("In Array {2, 3, 5, 15, 15, 16, 18, 21} Data 16 found at " + binarySearch(new int[]{2, 3, 5, 12, 15, 16, 18, 21}, 16));
		System.out.println("In Array {9, 12, 16, 18, 20, 22, 24} Data 9 found at " + binarySearch(new int[]{9, 12, 16, 18, 20, 22, 24}, 9));
		System.out.println("In Array {7, 7, 7, 7, 7, 7, 7, 7, 7, 7} Data 3 found at " + binarySearch(new int[]{7, 7, 7, 7, 7, 7, 7, 7, 7, 7}, 3));
		System.out.println("In Array {7, 7, 7, 7, 7, 7, 7, 7, 7, 7} Data 7 found at " + binarySearch(new int[]{7, 7, 7, 7, 7, 7, 7, 7, 7, 7}, 7));
	}
}