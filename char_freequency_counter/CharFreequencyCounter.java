import java.util.Arrays;

public class CharFreequencyCounter {
	// Method to count the frequency of the characters of the string
	static String countCharFrequency(String str) {
		// convert all characters to same case
		// sort the given string so as to arrange the characters consecutively according to their frequency
		// using the character indexes count and move to next character
		
		String resultStr = "";
		
		if (str == null || str.length() <= 0)
			return resultStr;
		
		// Sort the characters in the string, so that we get the frequent char consecutively
		char[] charArray = str.toLowerCase().toCharArray(); 
		Arrays.sort(charArray);
		String sortedStr = new String(charArray);
		
		// System.out.println("Sorted string " + sortedStr + " of length " + sortedStr.length());
		
		for(int index = 0, loop = 0; index < sortedStr.length() ; ) {
			int lastCharOccurrence = sortedStr.lastIndexOf(sortedStr.charAt(index));
			int charFrequency =  lastCharOccurrence - index + 1;
			
			resultStr = resultStr + charFrequency + sortedStr.charAt(index);
			
			// set the next loop index after the last occurrence of current character
			index = lastCharOccurrence + 1;
			// System.out.println(++loop);
		}
		
		return resultStr;
	}
	
	public static void main(String[] args) {
		System.out.println("Char frequency of null string is " + countCharFrequency(null));
		System.out.println("Char frequency of empty string is " + countCharFrequency(""));
		System.out.println("Char frequency of 'Googleee' is " + countCharFrequency("Googleee"));
		System.out.println("Char frequency of 'functor' is " + countCharFrequency("functor"));
		System.out.println("Char frequency of 'Basavaraj' is " + countCharFrequency("Basavaraj"));
		System.out.println("Char frequency of 'TtTtTtTtTtTtTtTt' is " + countCharFrequency("TtTtTtTtTtTtTtTt"));
	}
}
